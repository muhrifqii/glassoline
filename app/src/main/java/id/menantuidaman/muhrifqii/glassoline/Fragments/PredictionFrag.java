package id.menantuidaman.muhrifqii.glassoline.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import id.menantuidaman.muhrifqii.glassoline.GA.TimeSeries;
import id.menantuidaman.muhrifqii.glassoline.R;

/**
 * Created by muhrifqii on 11/30/2015.
 */
public class PredictionFrag extends Fragment {

    public static final String TAG = "PredictionFragmentClass";

    private ProgressBar predictProgressBar;
    private EditText inputYear;
    private FloatingActionButton predictButton;
//    private Switch advanceSwitch;
//    private RecyclerView advanceOptionRV;
    private TextView resTv;

    public PredictionFrag() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_prediction, container, false);
        root.setTag(TAG);
        initializeView(root);
        setListener();
        return root;
    }

    private void initializeView(View root){
        predictProgressBar = (ProgressBar) root.findViewById(R.id.predict_progress);
        predictButton = (FloatingActionButton) root.findViewById(R.id.fab_predict_ndes);
        inputYear = (EditText) root.findViewById(R.id.predict_year);
//        advanceSwitch = (Switch) root.findViewById(R.id.advance_switch);
//        advanceOptionRV = (RecyclerView) root.findViewById(R.id.recycler_view);
        resTv = (TextView) root.findViewById(R.id.result_tv);
    }

    private void setListener(){
//        advanceSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(!isChecked){
//                    advanceOptionRV.setVisibility(View.GONE);
//                }
//                else advanceOptionRV.setVisibility(View.VISIBLE);
//            }
//        });
        predictButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getContext(), "istighfar...", Toast.LENGTH_SHORT).show();
                Log.e("asem","asem");
                String toPredict = inputYear.getText().toString();
                if (!toPredict.isEmpty()) {
                    Log.e("click","notempty");
                    TimeSeries algo = TimeSeries.getCurrentTimeSeries();
                    if(!algo.getNewPopulation().isEmpty() || algo.getNewPopulation() != null){
                        Log.e("cekpopu","notempty");
                        double result = algo.predict(Integer.parseInt(toPredict));
                        Log.e("result",String.valueOf(result));
                        resTv.setText(String.valueOf(result));
                    }
                    else {
                        Toast.makeText(getContext(), "please go to train data first. this version is still for people with computer science major"
                        , Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

}

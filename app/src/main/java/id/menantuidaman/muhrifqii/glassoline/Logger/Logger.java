package id.menantuidaman.muhrifqii.glassoline.Logger;

import android.util.Log;

import java.util.List;

/**
 * Created by muhrifqii on 12/2/2015.
 */
public final class Logger {
    public static final int LOG_TYPE_ERROR = -1;
    public static final int LOG_TYPE_DEBUG = 0;
    public static final int LOG_TYPE_INFORMATION = 1;
    public static final int LOG_TYPE_WARNING = 2;

    public static void LogDoubleOnListHorizontally(String tag, int type, List<Double> obj, String bonus){
        String row = bonus;
        for (double val : obj){
            row += " " + String.valueOf(val);
        }
        switch (type){
            case LOG_TYPE_ERROR: Log.e(tag, row);
                break;
            case LOG_TYPE_DEBUG: Log.d(tag, row);
                break;
            case LOG_TYPE_INFORMATION: Log.i(tag, row);
                break;
            case LOG_TYPE_WARNING: Log.w(tag, row);
                break;
            default: Log.wtf(tag, row);
        }
    }

    public static void LogDoubleOnList(String tag, int type, List<Double> obj){
        for (double val : obj){
            switch (type){
                case LOG_TYPE_ERROR: Log.e(tag, String.valueOf(val));
                    break;
                case LOG_TYPE_DEBUG: Log.d(tag, String.valueOf(val));
                    break;
                case LOG_TYPE_INFORMATION: Log.i(tag, String.valueOf(val));
                    break;
                case LOG_TYPE_WARNING: Log.w(tag, String.valueOf(val));
                    break;
                default: Log.wtf(tag, String.valueOf(val));
            }
        }
    }
}

package id.menantuidaman.muhrifqii.glassoline;

/**
 * Created by muhrifqii on 11/30/2015.
 */
public final class MtrGasolineConsumData {
    /**
     * Indonesain gasoline consumption (in thousand barrels) per day from 1986 - 2012 (from eia.gov)
     */
    private static double[] indo = {
            77.41985, 83.78995, 89.577, 97.769, 109.68674, 117.9, 123.58, 128.029, 143.74122, 125.49386,
            163.23779, 173.75493, 174.38676, 208.01515, 213.80934, 234.82038, 255.15216, 270.01288, 292.59717,
            319.76984, 345.47208, 378.82548, 381.77577, 422.55049, 442.43825, 486.56055, 529.72468
    };

    public static int getDataSize(){
        return indo.length;
    }

    public static int getYearofLastData(){
        return 2012;
    }
    public static int getYearofFirstData(){
        return 1986;
    }

    public static double[] getArrayAllData(){
        return indo;
    }

    public static double[] getSomeData(int startin, int range){
        double[] x = new double[range];
        int p = 0;
        for (int i = startin; i < range; i++) {
            x[p] = indo[i];
            p++;
        }
        return x;
    }

    public static double[] getSomeReversedData(int last, int range){
        double[] x = new double[range];
        int p = 0;
        for (int i = last; i > last - range; i--) {
            x[p] = indo[i];
            p++;
        }
        return x;
    }

    public static double[] getSomeDataFromYears(int startingYears, int range){
        try {
            startingYears -= 1986; // minus the first year in Indonesia
            if(startingYears < 0) throw new Exception("negative value");
        } catch (Exception e) {
            e.printStackTrace();
//            startingYears = 0;
        }
        return getSomeData(startingYears, range);
    }

    public static double[] getSomeReversedDataFromYears(int endYear, int range){
        try {
            endYear -= 1986; // minus the first year in Indonesia
            if(endYear < 0) throw new Exception("negative val " + String.valueOf(endYear) + " " + String.valueOf(range));
        } catch (Exception e) {
            e.printStackTrace();
//            startingYears = 0;
        }
        return getSomeReversedData(endYear, range);
    }

    public static double getData(int index){
        if(index < 0) return -1;
        return indo[index];
    }

    public static double getDataFromYear(int year){
        try {
            year -= 1986; // minus the first year in Indonesia
            if(year < 0) throw new Exception("negative value");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getData(year);
    }
}

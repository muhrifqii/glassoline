package id.menantuidaman.muhrifqii.glassoline.GA;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import id.menantuidaman.muhrifqii.glassoline.ElitismMergeSort;
import id.menantuidaman.muhrifqii.glassoline.Logger.GenerationUpdateParse;
import id.menantuidaman.muhrifqii.glassoline.Model.Chromosome;
import id.menantuidaman.muhrifqii.glassoline.MtrGasolineConsumData;

/**
 * Created by muhrifqii on 11/30/2015.
 */
public class TimeSeries {
    public static final int GA_MAX_ITTER = 1000;
    public static final int POPULATION_SET = 8;
    public static final int CROMOSOME_SIZE = 7;
    public static final int LAST_YEAR_TO_CHECK = 2010;
    public static final double MIN_THRESHOLD = 0.1;
    public static final double PC = 0.7;
    public static final double PM = 0.05;
    public static final double ALPHA = 0.6;

    private static final Random rand = new Random();

    private static TimeSeries current;

    private static List<Chromosome> parents;
    private static List<Chromosome> offsprings;
    private static List<Chromosome> newPopulation;

    private static int iteration = GA_MAX_ITTER;
    private static int populationSize = POPULATION_SET;
    private static int chromoSize = CROMOSOME_SIZE;
    private static double pc = PC;
    private static double pm = PM;
    private static double alpha = ALPHA;

    private static Chromosome best;

    private static GenerationUpdateParse updaterString;

    private TimeSeries(){
        updaterString = GenerationUpdateParse.getCurrent();
    }

    /**
     * singleton TimeSeries Class
     * @return existing time series object
     */
    public static TimeSeries getCurrentTimeSeries(){
        if(current == null) current = new TimeSeries();
        return current;
    }

    public static int getIteration() {
        return iteration;
    }

    public static int getPopulationSize() {
        return populationSize;
    }

    public static int getChromoSize() {
        return chromoSize;
    }

    public static double getPc() {
        return pc;
    }

    public static double getPm() {
        return pm;
    }

    public static double getAlpha() {
        return alpha;
    }

    public static Chromosome getBest() {
        return best;
    }

    public static List<Chromosome> getNewPopulation(){
        return newPopulation;
    }

    /**
     * prediction
     * @param year year to predict
     * @return the gasoline prediction
     */
    public static double predict(int year){
        double _predict = newPopulation.get(0).getGen().get(0);
//        double[] data = MtrGasolineConsumData.getSomeReversedDataFromYears(LAST_YEAR_TO_CHECK, MtrGasolineConsumData.getDataSize() - 2);
        int yearToPredict = year;
        for (int j = 1; j < chromoSize; j++) {
            if(yearToPredict > MtrGasolineConsumData.getYearofLastData()){
                _predict += predict(yearToPredict-1) * newPopulation.get(0).getGen().get(j);
            }
            else{
                _predict += (newPopulation.get(0).getGen().get(j) * MtrGasolineConsumData.getDataFromYear(yearToPredict));
            }
            yearToPredict--;
        }

        return _predict;
    }

    /**
     * used for expert user only
     * @param popu population size
     * @param genSize gen size
     * @param _pc crossover probability
     * @param _pm mutation probability
     * @param _alpha alpha in single arithmatic crossover
     */
    public static void customMode(String iter, String popu, String genSize, String _pc, String _pm, String _alpha){
        if(!iter.isEmpty()){
            iteration = Integer.parseInt(iter);
        }
        if (!popu.isEmpty()) {
            populationSize = Integer.parseInt(popu);
        }
        if (!genSize.isEmpty()) {
            chromoSize = Integer.parseInt(genSize);
        }
        if (!_pc.isEmpty()) {
            pc = Double.parseDouble(_pc);
        }
        if (!_pm.isEmpty()) {
            pm = Double.parseDouble(_pm);
        }
        if (!_alpha.isEmpty()) {
            alpha = Double.parseDouble(_alpha);
        }
    }

    /**
     * train GA
     * @param year the year to train (add 1992)
     * @throws Exception
     */
    public static void train(int year) throws Exception {
        remove();
        if(year - MtrGasolineConsumData.getYearofFirstData() < 0
                && year - chromoSize + 1 < MtrGasolineConsumData.getYearofFirstData())
            throw new Exception("do not train before the first data");
        // size of population & startin year
        generateFirstPopulation(populationSize, year);
        for(int i=0;i< iteration;i++){
//            Log.e("GENERATION UPDATE", String.valueOf(i));//done
            updaterString.updateGeneration(i);
            mainLogic(year);
            if(validation(2011) && validation(2012)){
                best = newPopulation.get(0);
                break;
            }
        }
        best = newPopulation.get(0);
    }

    private static boolean validation(int year){
        updaterString.addKey("validation check with year " + String.valueOf(year));
        double predict = newPopulation.get(0).getGen().get(0);
        double[] data = MtrGasolineConsumData.getSomeReversedDataFromYears(year, chromoSize);

        for (int j = 1; j < chromoSize; j++) {
            predict += (newPopulation.get(0).getGen().get(j) * data[j-1]);
        }
        updaterString.addString("(data) " + String.valueOf(MtrGasolineConsumData.getDataFromYear(year))
                + " - (prediction) " + String.valueOf(predict) + " = " + String.valueOf(Math.abs(MtrGasolineConsumData.getDataFromYear(year) - predict)));
        if(Math.abs(MtrGasolineConsumData.getDataFromYear(year) - predict) < MIN_THRESHOLD){
            return true;
        }
        return false;
    }

    /**
     * reset everything
     */
    protected static void remove(){
        parents = new ArrayList<>();
        offsprings = new ArrayList<>();
        newPopulation = new ArrayList<>();
        updaterString.reNew();
    }

    /**
     * random list of double values
     * @param size the list size
     * @return the new random list
     */
    protected static List<Double> randomizeSet(int size){
        List<Double> ret = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            ret.add((double) rand.nextInt(10000) / 10000);
        }
        return ret;
    }

    /**
     * iteration begin
     * @param year year
     */
    private static void mainLogic(int year){
        Log.d("mainLogic","in");
        double sum = 0;
        double[] data = MtrGasolineConsumData.getSomeReversedDataFromYears(LAST_YEAR_TO_CHECK, MtrGasolineConsumData.getDataSize() - 2);
        parents = new ArrayList<>(newPopulation);
        for (int i = 0; i < populationSize; i++) {
            parents.get(i).setFitnessVal(fitnessFunction(parents.get(i).getGen(), data, year));
            sum += parents.get(i).getFitnessVal();
            Log.d("PARENT", String.valueOf(i) + " -> " + String.valueOf(parents.get(i).getFitnessVal()));
        }
        calcFitProb(sum);
        calcCumProb();

        updaterString.addKey("fitness evaluation # fitness # fitness prob # cumulative prob");
        for (Chromosome one : parents){
            String kk = "";
            for (double dob : one.getGen()){
                kk += String.valueOf(dob);
                kk += " ";
            }
            kk += "# " + String.valueOf(one.getFitnessVal())+ " # " +
                    String.valueOf(one.getFitnessProb()) + " # " + String.valueOf(one.getCumulativeProb());
            updaterString.addString(kk);
//            Log.d("parents",kk);//addstring
        }
        offsprings = new ArrayList<>(uniformRandomMutation(singleArithmaticrossOver(RWS())));
        // evaluate ofs fitness
//        sum = 0;
        for (int i = 0; i < offsprings.size(); i++) {
            offsprings.get(i).setFitnessVal(fitnessFunction(offsprings.get(i).getGen(), data, year));
//            sum += offsprings.get(i).getFitnessVal();
        }
        updaterString.addKey("offspring # fitness");
        for (Chromosome one : offsprings){
            String kk = "";
            for (double dob : one.getGen()){
                kk += String.valueOf(dob);
                kk += " ";
            }
            kk += "# " + String.valueOf(one.getFitnessVal());
            Log.d("offspring", kk);//addstring
        }
        newPopulation = new ArrayList<>(elitism(parents, offsprings));
        for (int i = 0; i < newPopulation.size(); i++) {
            Log.d("NEW POPULATION", String.valueOf(i) + " - " + newPopulation.get(i).getFitnessVal());
        }
    }

    /**
     * random first population
     * @param population size of population
     * @param year the year
     */
    private static void generateFirstPopulation(int population, int year){
        Log.d("generateFirstPopulation","in");
        parents = new ArrayList<>();
        double sum = 0;
        double[] data = MtrGasolineConsumData.getSomeReversedDataFromYears(LAST_YEAR_TO_CHECK, MtrGasolineConsumData.getDataSize() - 2);
        for (int i = 0; i < population; i++) {
            parents.add(new Chromosome(randomizeSet(chromoSize)));
            parents.get(i).setFitnessVal(fitnessFunction(parents.get(i).getGen(), data, year));
            sum += parents.get(i).getFitnessVal();
            Log.d("PARENT", String.valueOf(i) + " -> " + String.valueOf(parents.get(i).getFitnessVal()));
        }
//        "first random population"
        updaterString.addKey("first random population");
        for (Chromosome one : parents){
            String kk = "";
            for (double dob : one.getGen()){
                kk += String.valueOf(dob);
                kk += " ";
            }
            updaterString.addString(kk);
//            Log.d("parents",kk);//addstring

        }
        calcFitProb(sum);
        calcCumProb();
        updaterString.addKey("fitness evaluation # fitness # fitness prob # cumulative prob");
        for (Chromosome one : parents){
            String kk = "";
            for (double dob : one.getGen()){
                kk += String.valueOf(dob);
                kk += " ";
            }
            kk += "# " + String.valueOf(one.getFitnessVal())+ " # " +
                    String.valueOf(one.getFitnessProb()) + " # " + String.valueOf(one.getCumulativeProb());
            updaterString.addString(kk);
//            Log.d("parents",kk);//addstring

        }
        offsprings = new ArrayList<>(uniformRandomMutation(singleArithmaticrossOver(RWS())));
        // evaluate ofs fitness
        for (int i = 0; i < offsprings.size(); i++) {
            offsprings.get(i).setFitnessVal(fitnessFunction(offsprings.get(i).getGen(), data, year));
        }
        updaterString.addKey("offspring # fitness");
        for (Chromosome one : offsprings){
            String kk = "";
            for (double dob : one.getGen()){
                kk += String.valueOf(dob);
                kk += " ";
            }
            kk += "# " + String.valueOf(one.getFitnessVal());
            Log.d("offspring", kk);//addstring
        }
        newPopulation = new ArrayList<>(elitism(parents, offsprings));
    }

    /**
     * to calculate the fitness probability
     * @param sum the sum of all fitness
     */
    private static void calcFitProb(double sum){
        for (Chromosome kromo : parents) {
            kromo.setFitnessProb(kromo.getFitnessVal()/sum);
//            Log.d("fitness probability",String.valueOf(kromo.getFitnessVal()/sum));
        }
    }

    /**
     * to calculate the cumulative probability
     */
    private static void calcCumProb(){
        parents.get(0).setCumulativeProb(parents.get(0).getFitnessProb());
        for (int i = 1; i < populationSize; i++) {
            parents.get(i).setCumulativeProb(parents.get(i).getFitnessProb() + parents.get(i - 1).getCumulativeProb());
        }
    }

    /**
     * [not a GA function] function for duplicating the list, because type reference of List cant explicitly copy the value of Lisst
     * @param listToCopy
     * @return the new duplicated List
     */
    private static List<Double> copyList(List<Double> listToCopy){
        List<Double> newList = new ArrayList<>();
        for (double num : listToCopy){
            newList.add(num);
        }
        return newList;
    }


    // TODO: 12/9/2015 first step to check after finish the hotfix issues
    /**
     * RWS Function for selecting parent
     * @return the list of Cromosome that is selected as parents after being RWS-ed
     */
    private static List<Chromosome> RWS(){
        Log.d("RWS","in");

        List<Chromosome> rws = new ArrayList<>();
        List<Double> randoman = randomizeSet(populationSize);
        for (double aRandom : randoman){
            for (int i = populationSize - 1; i >= 0; i--) {
                if(aRandom > parents.get(i).getCumulativeProb()){
                    if(!parents.get(i + 1).isAdded()) {
                        rws.add(parents.get(i + 1));
                        parents.get(i + 1).setAdded(true);
                    }
                }
                else if(i == 0 && !parents.get(0).isAdded()) {
                    rws.add(parents.get(0));
                    parents.get(0).setAdded(true);
                }
            }
        }
        String tmmp = "";
        updaterString.addKey("rws random number");
        for (double dob : randoman) {
            tmmp += String.valueOf(dob);
            tmmp += " ";
        }
        updaterString.addString(tmmp);
        for (Chromosome one : rws){
            String kk = "";
            for (double dob : one.getGen()){
                kk += String.valueOf(dob);
                kk += " ";
            }
            kk += "fitness:" + String.valueOf(one.getFitnessVal());
            Log.d("rws", kk);
        }
        return rws;
    }

    // TODO: 12/9/2015 the bug not yet come up, still debuggin
    /**
     * by using probability crossover, function of single arithmatic crossover
     * @param tmpParentToCrossOver list of cromosomes that is selected to be the parent
     * @return the new offspring
     */
    private static List<Chromosome> singleArithmaticrossOver(List<Chromosome> tmpParentToCrossOver){
        Log.d("crossover", "in");
        List<Double> randoman = randomizeSet(tmpParentToCrossOver.size());
        List<Chromosome> selectedParent = new ArrayList<>();
        List<Chromosome> theOffspring = new ArrayList<>();
        updaterString.addKey("random number for pc");
        String tmpp = "";
        for (int i = 0; i < randoman.size(); i++) {
//            Log.d("randoman", String.valueOf(randoman.get(i)));
            tmpp += String.valueOf(randoman.get(i)) + " ";
            if(randoman.get(i) < pc) selectedParent.add(tmpParentToCrossOver.get(i));
        }
        updaterString.addString(tmpp);

        if(selectedParent.size() < 1) return selectedParent;

        updaterString.addKey("selected parent");
        for (Chromosome one : selectedParent){
            String kk = "";
            for (double dob : one.getGen()){
                kk += String.valueOf(dob);
                kk += " ";
            }
            updaterString.addString(kk);
//            Log.d("after pc",kk);
        }

        // crossover combination
        for (int i = 0; i < selectedParent.size(); i++) {
            for (int j = i + 1; j < selectedParent.size(); j++) {
                int lolIndex = rand.nextInt(chromoSize);
                updaterString.addKey("crossover p" + String.valueOf(i) + " x p" + String.valueOf(j) + " crossover index " + String.valueOf(lolIndex));
                List<Double> x = copyList(selectedParent.get(i).getGen());
                List<Double> y = copyList(selectedParent.get(j).getGen());
                double nextX = alpha * y.get(lolIndex) + (1 - alpha) * x.get(lolIndex);
                double nextY = alpha * x.get(lolIndex) + (1 - alpha) * y.get(lolIndex);
                // overwrite the lol index
                x.set(lolIndex, nextX);
                y.set(lolIndex, nextY);
                tmpp = "";
                for (double dob : x) {
                    tmpp += String.valueOf(dob) + " ";
                }
                tmpp += "\n";
                for (double dob : y) {
                    tmpp += String.valueOf(dob) + " ";
                }
                updaterString.addString(tmpp);
                theOffspring.add(new Chromosome(x));
                theOffspring.add(new Chromosome(y));
            }
        }
//        for (Chromosome one : theOffspring){
//            String kk = "";
//            for (double dob : one.getGen()){
//                kk += String.valueOf(dob);
//                kk += " ";
//            }
//            Log.d("after crossover",kk);
//        }

        return theOffspring;
    }

    /**
     * mutation function using uniform random mutation
     * @param offsprings the list of offspring
     * @return the mutated list of offspring
     */
    private static List<Chromosome> uniformRandomMutation(List<Chromosome> offsprings){
        Log.d("mutation", "in");
        if(offsprings.size() == 0)
            return offsprings;
        int totalMutatedGen = (int)((double)(chromoSize * populationSize) * pm);
        int[] randomIndex = new int[totalMutatedGen];
        double randomForProductMutation = rand.nextDouble();
        updaterString.addKey(String.valueOf(totalMutatedGen) + " selected gen mutation with generated random number: " + String.valueOf(randomForProductMutation));
        Log.d("see val", String.valueOf(randomForProductMutation) + " " + String.valueOf(offsprings.size()));
        for (int i = 0;i < totalMutatedGen; i++) {
            randomIndex[i] = rand.nextInt(chromoSize * offsprings.size());
            for (int j = 0; j < offsprings.size(); j++) {
//                Log.e("uniformRandom", String.valueOf(randomIndex[i]));
                String kk = "";
                if(randomIndex[i] < chromoSize){
                    double tmp = offsprings.get(j).getGen().get(randomIndex[i]);
                    offsprings.get(j).getGen().set(randomIndex[i], tmp * randomForProductMutation);
                    for (double dob : offsprings.get(j).getGen()) {
                        kk += String.valueOf(dob) + " ";
                    }
                    updaterString.addKey(kk);
                    break;
                }
                randomIndex[i] -= chromoSize;
            }
        }
        return offsprings;
    }

    /**
     * selecting survivor by combining parent and offspring using elitism
     * @param parent list of parent
     * @param offspring list of offspring
     * @return better generation
     */
    private static List<Chromosome> elitism(List<Chromosome> parent, List<Chromosome> offspring){
        Log.d("elitism","in");
        List<Chromosome> newGen = new ArrayList<>();
        Log.d("see size", String.valueOf(parent.size()) + " " + String.valueOf(offspring.size()));
        newGen.addAll(parent);
        newGen.addAll(offspring);
        int size = newGen.size();
        for (int i = 0; i < size; i++) {
            Log.d("list before elitism", String.valueOf(newGen.get(i).getFitnessVal()));
        }
        ElitismMergeSort merger = new ElitismMergeSort(newGen);
//        newGen.removeAll(newGen);
        newGen = new ArrayList<>();
        for (int i = 0; i < populationSize; i++) {
            newGen.add(merger.getSortedArray().get(i));
        }
//        for (int i = size - 1; i >= size - populationSize; i--) {
//            newGen.add(merger.getSortedArray().get(i));
//        }
        for (Chromosome one : newGen){
            String kk = "";
//            for (double dob : one.getGen()){
//                kk += String.valueOf(dob);
//                kk += " ";
//            }
            Log.d("list after elitism",String.valueOf(one.getFitnessVal()));
        }
        return newGen;
    }

    /**
     * fitness function by calculating Mean Absolute Deviation
     * @param gen list of gen
     * @param data data of gasoline consumption
     * @param year the year
     * @return the fitness value
     */
    private static double fitnessFunction(List<Double> gen, double[] data, int year){
        double fitness = 0;
        // Mean Absolute Deviation before average
        double dif;

        double predict = gen.get(0);
        // evaluate trough all data training
        for (int i = Math.abs(year - MtrGasolineConsumData.getYearofFirstData());
             i < Math.abs(MtrGasolineConsumData.getYearofFirstData() - LAST_YEAR_TO_CHECK); i++) {
            int k = i;

            for (int j = 1; j < gen.size(); j++) {
//                predict += (gen.get(j) * data[j-1]);
//                Log.d("K&J",String.valueOf(k) + " " + String.valueOf(j));
                predict += (gen.get(j) * MtrGasolineConsumData.getData(k--));
            }
            dif = Math.abs(MtrGasolineConsumData.getData(i) - predict);
            fitness += dif;
            predict = gen.get(0);
        }
        
        return 1/(fitness/Math.abs(year - LAST_YEAR_TO_CHECK));
    }

}

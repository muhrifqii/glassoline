package id.menantuidaman.muhrifqii.glassoline.Fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import id.menantuidaman.muhrifqii.glassoline.GA.TimeSeries;
import id.menantuidaman.muhrifqii.glassoline.Logger.GenerationUpdateParse;
import id.menantuidaman.muhrifqii.glassoline.R;

/**
 * Created by muhrifqii on 12/1/2015.
 */
public class TrainFrag extends Fragment {

    public static final String TAG = "TrainFragmentClass";
    private GenerationUpdateParse result;

    private FloatingActionButton trainButton;
//    private RecyclerView recyclerView;
    private TimeSeries timeSeries;
    private ProgressBar progressBar;
    private LinearLayout llScroller;
    private EditText cstmPopSize, cstmGenSize, cstmPC, cstmPM, cstmAlpha, iter;
    private long startTime;
    private long endTime;
    private long duration;

    public TrainFrag() {
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_train, container, false);
        root.setTag(TAG);
        initializeView(root);
        setListener();
        return root;
    }
    private void initializeView(View root){
        trainButton = (FloatingActionButton) root.findViewById(R.id.fab_train);
//        recyclerView = (RecyclerView) root.findViewById(R.id.recycler_view);
        progressBar = (ProgressBar) root.findViewById(R.id.progress_bar);
        llScroller = (LinearLayout) root.findViewById(R.id.insideScroller);
        cstmPopSize = (EditText) root.findViewById(R.id.popsize);
        cstmGenSize = (EditText) root.findViewById(R.id.cromosize);
        cstmPC = (EditText) root.findViewById(R.id.pc_et);
        cstmPM = (EditText) root.findViewById(R.id.pm_et);
        cstmAlpha = (EditText) root.findViewById(R.id.alpha_et);
        iter = (EditText) root.findViewById(R.id.iteration);

//        LinearLayoutManager llm = new LinearLayoutManager(getContext());
//        recyclerView.setLayoutManager(llm);
    }

    private void setListener(){
        trainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeSeries = TimeSeries.getCurrentTimeSeries();
                timeSeries.customMode(iter.getText().toString(), cstmPopSize.getText().toString(), cstmGenSize.getText().toString(), cstmPC.getText().toString(),
                        cstmPM.getText().toString(), cstmAlpha.getText().toString());
                try {
                    new theTask().execute(new Integer[]{1992});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private class theTask extends AsyncTask<Integer, Void, TimeSeries>{
        @Override
        protected void onPostExecute(TimeSeries timeSeries) {
            progressBar.setVisibility(View.GONE);

            endTime = System.nanoTime();
            duration = (endTime - startTime)/1000000;

            for (String one :result.getCurrent().getDataPerGeneration()) {
                TextView tv = new TextView(getContext());
                tv.setTextSize(12);
                tv.setText(one);
                llScroller.addView(tv);
            }
            new AlertDialog.Builder(getContext())
                    .setTitle("GA Train Process")
                    .setMessage("train duration is: " + String.valueOf(duration)  + " ms\n"
                            + "this process is done on different thread, other than UI thread. lack performance rise from UI Thread")
                    .setPositiveButton("OK", null)
                    .create()
                    .show();
            new AlertDialog.Builder(getContext())
                    .setMessage("this is beta version, you are currently testing the \"must done apps in a team\" for final project on GA got from Lecturer Ms Faizah\n"
                            + "big thanks to everyone who supports this project. special thanks to Ms Faizah, so we can implement GA and made this app. and thanks for everyone whose trying and testing this application")
                    .setPositiveButton("OK", null)
                    .create()
                    .show();
            cstmAlpha.setText(String.valueOf(timeSeries.getAlpha()));
            cstmGenSize.setText(String.valueOf(timeSeries.getChromoSize()));
            cstmPC.setText(String.valueOf(timeSeries.getPc()));
            cstmPM.setText(String.valueOf(timeSeries.getPm()));
            cstmPopSize.setText(String.valueOf(timeSeries.getPopulationSize()));
            iter.setText(String.valueOf(timeSeries.getIteration()));

            super.onPostExecute(timeSeries);
        }

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            startTime = System.nanoTime();

            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected TimeSeries doInBackground(Integer... params) {
            try {
                timeSeries.train(params[0].intValue());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return timeSeries;
        }
    }
}

package id.menantuidaman.muhrifqii.glassoline.Model.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by muhrifqii on 12/1/2015.
 */
public class GenerationAdapter extends RecyclerView.Adapter<GenerationAdapter.GenerationViewHolder> {

    private List<Double> data;

    public GenerationAdapter(Context context, List<Double> data) {
        this.data = data;
    }

    @Override
    public GenerationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(GenerationViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class GenerationViewHolder extends RecyclerView.ViewHolder{
        public GenerationViewHolder(View item){
            super(item);
        }


    }
}

package id.menantuidaman.muhrifqii.glassoline.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.Switch;

import id.menantuidaman.muhrifqii.glassoline.R;

/**
 * Created by muhrifqii on 12/1/2015.
 */
public class HomeFrag extends Fragment {
    public static final String TAG = "HomeFragmentClass";

    public HomeFrag() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_prediction, container, false);
        root.setTag(TAG);
        initializeView(root);
        return root;
    }
    private void initializeView(View root){
//        predictProgressBar = (ProgressBar) root.findViewById(R.id.predict_progress);
//        predictButton = (FloatingActionButton) root.findViewById(R.id.fab_predict);
//        inputYear = (AutoCompleteTextView) root.findViewById(R.id.predict_year);
//        advanceSwitch = (Switch) root.findViewById(R.id.advance_switch);
//        advanceOptionRV = (RecyclerView) root.findViewById(R.id.recycler_view);

    }
}

package id.menantuidaman.muhrifqii.glassoline.Model;

import java.util.List;

/**
 * Created by muhrifqii on 12/1/2015.
 */
public class Chromosome {
    private List<Double> gen;
    private double fitnessVal;
    private double fitnessProb;
    private double cumulativeProb;
    private boolean added = false;

    public Chromosome(List<Double> gen){
        this.gen = gen;
    }

    public double getFitnessVal() {
        return fitnessVal;
    }

    public void setFitnessVal(double fitnessVal) {
        this.fitnessVal = fitnessVal;
    }

    public double getFitnessProb() {
        return fitnessProb;
    }

    public void setFitnessProb(double fitnessProb) {
        this.fitnessProb = fitnessProb;
    }

    public double getCumulativeProb() {
        return cumulativeProb;
    }

    public void setCumulativeProb(double cumulativeProb) {
        this.cumulativeProb = cumulativeProb;
    }

    public boolean isAdded() {
        return added;
    }

    public void setAdded(boolean added) {
        this.added = added;
    }

    public List<Double> getGen() {
        return gen;
    }
}

package id.menantuidaman.muhrifqii.glassoline.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muhrifqii on 12/14/2015.
 */
public class GenerationUpdateParse {
    private static List<String> dataPerGeneration;
    private static GenerationUpdateParse current;
    private static String temporary;

    private GenerationUpdateParse(){
        dataPerGeneration = new ArrayList<>();
        temporary = "";
    }

    public static GenerationUpdateParse getCurrent(){
        if (current == null) {
            current = new GenerationUpdateParse();
        }
        return current;
    }

    public static void reNew(){
        current = new GenerationUpdateParse();
    }

    public static void updateGeneration(int itterNumber){
        dataPerGeneration.add(itterNumber,temporary);
        temporary = "-----RESULT GENERATION UPDATE " + String.valueOf(itterNumber + 1) + "-----\n";
        dataPerGeneration.add(itterNumber+1, temporary);
    }

    public static void addKey(String keyword){
        temporary += keyword.toUpperCase();
        temporary += "\n";
    }

    public static void addString(String stringToAdd){
        temporary += stringToAdd;
        temporary += "\n";
    }

    public static List<String> getDataPerGeneration(){
        return dataPerGeneration;
    }
}

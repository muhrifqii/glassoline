package id.menantuidaman.muhrifqii.glassoline;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import id.menantuidaman.muhrifqii.glassoline.Model.Chromosome;

/**
 * Created by muhrifqii on 12/2/2015.
 */
public class ElitismMergeSort {
    private List<Chromosome> output;
    private List<Chromosome> helper;

    public List<Chromosome> getSortedArray() {
        return output;
    }

    public ElitismMergeSort(List<Chromosome> input) {
        output = new ArrayList<>();
        helper = new ArrayList<>();
        for (Chromosome one : input){
            List<Double> newGen = new ArrayList<>();
            for (int i = 0; i < one.getGen().size(); i++) {
                newGen.add(one.getGen().get(i));
            }
            Chromosome theNew = new Chromosome(newGen);
            theNew.setFitnessVal(one.getFitnessVal());
            theNew.setAdded(false);

            output.add(theNew);
        }
        sortGivenArray();
        for (Chromosome one : output){
//            String kk = "";
//            for (double dob : one.getGen()){
//                kk += String.valueOf(dob);
//                kk += " ";
//            }
//            Log.d("after crossover",kk);
//            Log.e("lookup fitness", String.valueOf(one.getFitnessVal()));
        }
    }

    public void sortGivenArray() {
        divide(0, this.output.size() - 1);
    }

    public void divide(int startIndex, int endIndex) {

        //Divide till you breakdown your list to single element
        if (startIndex < endIndex) {
            int mid = (endIndex + startIndex) / 2;
            divide(startIndex, mid);
            divide(mid + 1, endIndex);
            //merging Sorted array produce above into one sorted array
            merger(startIndex, mid, endIndex);
        }
    }

    public void merger(int startIndex, int midIndex, int endIndex) {

        for (int i = startIndex; i <= endIndex; i++) {
            helper.add(i, output.get(i));
        }

        int leftIndex = startIndex;
        int rightIndex = midIndex + 1;
        int current = startIndex;

        while (leftIndex <= midIndex && rightIndex <= endIndex) {
            // cek fitness
            if (helper.get(leftIndex).getFitnessVal() >= helper.get(rightIndex).getFitnessVal()) {
                output.set(current, helper.get(leftIndex));
                leftIndex++;
            } else {
                output.set(current, helper.get(rightIndex));
                rightIndex++;
            }
            current++;
        }
        while (leftIndex <= midIndex) {
            output.set(current, helper.get(leftIndex));
            current++;
            leftIndex++;
        }
//        int remaining = midIndex - leftIndex;
//        for(int j=0; j <= remaining; j++) {
//            output.set(current+j, helper.get(leftIndex+j));
//        }
    }
}
